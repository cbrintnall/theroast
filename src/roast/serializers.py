from rest_framework import serializers
from roast.models import Roast

class RoastSerializer(serializers.ModelSerializer):
    def create(self, data):
        obj = super(RoastSerializer, self).create(data)
        print("hi")
        return obj

    class Meta:
        model = Roast
        fields = ('id', 'name', 'color', 'description')